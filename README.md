.cfg Files
==========
My configuration files for Counter-Strike: Global Offensive. Feel free to use them yourself and modify as you like. Mostly copied from [TrilluXe](http://schweineaim.com/trilluxe/).

Trademark information
=====================
Counter-Strike: Global Offensive, CS:GO, Steam and all related trademarks are registered trademarks of [Valve Corporation](https://valvesoftware.com).